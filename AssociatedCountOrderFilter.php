<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class AssociatedCountOrderFilter extends AbstractContextAwareFilter
{
    private $searchParameterName;

    /**
     * Add configuration parameter
     * {@inheritdoc}
     * @param string $searchParameterName The parameter whose value this filter searches for
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack = null,
        LoggerInterface $logger = null,
        array $properties = null,
        NameConverterInterface $nameConverter = null,
        string $searchParameterName = 'orderFilter'
    )
    {
        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);

        $this->searchParameterName = $searchParameterName;
    }

    /** {@inheritdoc} */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
        if (null === $value || $property !== $this->searchParameterName) {
            return;
        }
//        $alias = $queryBuilder->getRootAliases()[0];

//        if(is_array($value)) {
//            foreach ($value as $key => $val) {
//                $queryBuilder
//                    ->leftJoin("$alias.$key", $key."_order_children")
//                    ->addOrderby("COUNT(".$key."_order_children)", $val)
//                    ->groupBy($alias); // important to group by alias to show all
//            }
//        }

        /* IMPORTANT NOTE
         * subresource need have set fetch="EXTRA_LAZY" to display all of them when using "groupBy";
         */
        $alias = $queryBuilder->getRootAliases()[0];
        foreach ($value as $key => $val) {
            $field = $key;
            if ($this->isPropertyNested($field, $resourceClass)) {
                [$alias2, $field, $associations] = $this->addJoinsForNestedProperty($field, $alias, $queryBuilder, $queryNameGenerator, $resourceClass, Join::LEFT_JOIN);

                $queryBuilder->addGroupBy("$alias.id")
                    ->leftJoin("$alias2.$field", $field."_order_children")
                    ->addOrderby("COUNT(".$field."_order_children )","$val");
            } else {
                $queryBuilder
                    ->leftJoin("$alias.$key", $key."_order_children")
                    ->addOrderby("COUNT(".$key."_order_children)", $val)
                    ->groupBy($alias); // important to group by alias to show all
            }
        }
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$this->searchParameterName."[".$property."]"] = [
                'property' => $property,
                'required' => false,
                'type' => Type::BUILTIN_TYPE_STRING,
                'schema' => [
                    'type' => Type::BUILTIN_TYPE_STRING,
                    'enum' => ['asc', 'desc'],
                ],
                'swagger' => [
                    'description' => 'Find by count associated',
                ],
            ];
        }

        return $description;
    }
}
