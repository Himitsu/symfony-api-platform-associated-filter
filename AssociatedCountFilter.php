<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryBuilderHelper;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class AssociatedCountFilter extends AbstractContextAwareFilter
{
    private $searchParameterName;

    /**
     * Add configuration parameter
     * {@inheritdoc}
     * @param string $searchParameterName The parameter whose value this filter searches for
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack = null,
        LoggerInterface $logger = null,
        array $properties = null,
        NameConverterInterface $nameConverter = null,
        string $searchParameterName = 'countFilter'
    )
    {
        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);

        $this->searchParameterName = $searchParameterName;
    }

    /** {@inheritdoc} */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
        if (null === $value || $property !== $this->searchParameterName) {
            return;
        }

        /* IMPORTANT NOTE
         * subresource need have set fetch="EXTRA_LAZY" to display all of them when using "groupBy";
         */
        $alias = $queryBuilder->getRootAliases()[0];
        foreach ($value as $key => $val) {
            $field = $key;
            if ($this->isPropertyNested($field, $resourceClass)) {
                [$alias2, $field, $associations] = $this->addJoinsForNestedProperty($field, $alias, $queryBuilder, $queryNameGenerator, $resourceClass, Join::LEFT_JOIN);
                $tmpKey = str_replace(".", "_", $key);
                $queryBuilder->addGroupBy("$alias.id")
                    ->having("COUNT($alias2.$field) = :val_$tmpKey")
                    ->setParameter("val_$tmpKey", $val);
            } else {
                $tmpKey = str_replace(".", "_", $key);
                $queryBuilder
                    ->andWhere("SIZE($alias.$key) = :val_$tmpKey")
                    ->setParameter("val_$tmpKey", $val);
            }
        }
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$this->searchParameterName."[".$property."]"] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'swagger' => [
                    'description' => 'Find by count associated',
                ],
            ];
        }

        return $description;
    }
}
