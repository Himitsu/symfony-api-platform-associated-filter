# Symfony Api Platform Associated Filter

You can filter and order by count on associated resources even with nested properties.

Problem that I encountered.
https://stackoverflow.com/questions/71019418/api-platform-filtration-group-by-issue

## How to use

```
#[ApiFilter(AssociatedCountFilter::class, properties: [
    'seasons.episodes.video',
    'seasons'
])]
#[ApiFilter(AssociatedCountOrderFilter::class, properties: [
    'seasons.episodes.video',
    'seasons'
])]
```

Feel free to use or edit for your own needs.
