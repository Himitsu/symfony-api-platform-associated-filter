<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Filter\AssociatedCountFilter;
use App\Filter\AssociatedCountOrderFilter;
use App\Repository\FormatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=FormatRepository::class)
 * @ORM\EntityListeners({"App\EventListener\FormatCategorySetTypeListener"})
 * @Assert\Callback("validate")
 */
#[ApiFilter(AssociatedCountFilter::class, properties: [
    'seasons.episodes.video',
    'seasons'
])]
#[ApiFilter(AssociatedCountOrderFilter::class, properties: [
    'seasons.episodes.video',
    'seasons'
])]
class Format
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"format:read", "format:write"})
     * @Groups({"episode:read", "episode:write"})
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"format:read", "format:write"})
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Season::class, mappedBy="format", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @Groups({"format:read", "format:write"})
     */
    #[ApiSubresource]
    private $seasons;

    public function __construct()
    {
        $this->seasons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Season[]
     */
    public function getSeasons(): Collection
    {
        return $this->seasons;
    }

    public function addSeason(Season $season): self
    {
        if (!$this->seasons->contains($season)) {
            $this->seasons[] = $season;
            $season->setFormat($this);
        }

        return $this;
    }

    public function removeSeason(Season $season): self
    {
        if ($this->seasons->removeElement($season)) {
            // set the owning side to null (unless already changed)
            if ($season->getFormat() === $this) {
                $season->setFormat(null);
            }
        }

        return $this;
    }
}
